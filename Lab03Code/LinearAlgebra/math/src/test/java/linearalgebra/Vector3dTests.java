// Fred-Dgennie Louis 2036915
package linearalgebra;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class Vector3dTests {
    
    
    @Test
    public void testGet() {

        Vector3d vectorTest = new Vector3d(1, 2, 3);
        assertEquals(1.0, vectorTest.getX(), 0);
        assertEquals(2.0, vectorTest.getY(), 0);
        assertEquals(3.0, vectorTest.getZ(), 0);
    }

    @Test
    public void testMagnitude() {

        Vector3d vectorTest = new Vector3d(1, 1, 2);
        assertEquals(2.449489742783178, vectorTest.magnitude(), 0);
    }

    @Test
    public void testDotProduct() {

        Vector3d vectorTest1 = new Vector3d(1, 2, 3);
        Vector3d vectorTest2 = new Vector3d(4, 5, 6);

        assertEquals(32.0, vectorTest1.dotProduct(vectorTest2), 0);

    }

    @Test
    public void testAdd() {

        Vector3d vectorTest1 = new Vector3d(2, 6, 8);
        Vector3d vectorTest2 = new Vector3d(3, 4, 7);

        Vector3d vectorExpected = new Vector3d(5, 10, 15);
        Vector3d vectorActual = vectorTest1.add(vectorTest2);

        //testing fields by field
        assertEquals(vectorExpected.getX(), vectorActual.getX(), 0);
        assertEquals(vectorExpected.getY(), vectorActual.getY(), 0);
        assertEquals(vectorExpected.getZ(), vectorActual.getZ(), 0);

       
    }

}
