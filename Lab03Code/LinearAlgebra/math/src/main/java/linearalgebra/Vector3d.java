//Fred-Dgennie Louis 2036915
//https://gitlab.com/Fred-D/lab03.git 

package linearalgebra;

public class Vector3d {
    
    private double x;
    private double y;
    private double z;

    //contructor
    public Vector3d(double inputX, double inputY, double inputZ) {
        this.x = inputX;
        this.y = inputY;
        this.z = inputZ;
    }

    //get methods

    public double getX() {
        return this.x;
    }
    
    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    //other methods

    public double magnitude() {
        
        double vectorMagnitude = (this.x * this.x) + (this.y * this.y) + (this.z * this.z);
        return Math.sqrt(vectorMagnitude);

    }

    public double dotProduct(Vector3d vector) {
        
        double vectorDotProduct = (this.x * vector.x) + (this.y * vector.y) + (this.z * vector.z) ;
        return vectorDotProduct;
    }

    public Vector3d add(Vector3d inputVector) {
        
        double newX = this.x + inputVector.x;
        double newY = this.y + inputVector.y;
        double newZ = this.z + inputVector.z;

        Vector3d newVector = new Vector3d(newX, newY, newZ);

        return newVector;
    }
}
